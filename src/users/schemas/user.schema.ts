import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { AdminRoles, Roles } from '../interfaces';

export type UserDocument = User & Document;

@Schema({
  timestamps: {
    updatedAt: 'updated_at',
    createdAt: 'created_at',
  },
})
export class User {
  @Prop()
  password: string;
  @Prop()
  email: string;
  @Prop()
  is_activated: boolean;

  @Prop()
  role: Roles;
  @Prop()
  admin_role: AdminRoles;
  id: string;
}

export const UserSchema = SchemaFactory.createForClass(User);

UserSchema.set('toJSON', {
  virtuals: true,
  transform(doc, ret) {
    ret.id = doc._id.toString();
    delete ret._id;
    delete ret.__v;
    return ret;
  },
});

import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import * as config from 'config';
import * as crypto from 'crypto';
import { Model } from 'mongoose';
import { from, Observable } from 'rxjs';
import { User } from '../schemas/user.schema';

@Injectable()
export class UserService {
  constructor(@InjectModel('User') private userModel: Model<User>) {}

  /**
   * Find a user by username
   * @param username
   */
  findByUsername(username: string): Observable<User> {
    return from(this.userModel.findOne({ username }));
  }

  /**
   * Find a user by email and password ( not hashed in entry )
   * @param email
   * @param password
   */
  findByEmailAndPassword(email: string, password: string): Observable<User> {
    return from(
      this.userModel
        .findOne({
          email,
          password: this.hashPassword(password),
        })
        .select('-password'),
    );
  }

  private hashPassword(password: string): string {
    const hash = crypto.createHash(config.get('security.hashing_algorithm'));
    return hash.update(password, 'utf8').digest('hex');
  }

  findById(id: string): Observable<User> {
    return from(this.userModel.findById(id.toString()));
  }
}

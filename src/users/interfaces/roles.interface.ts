export enum Roles {
  Etudiant = 'Etudiant',
  Ancien = 'Ancien',
  Professeur = 'Professeur',
}

export enum AdminRoles {
  Membre = 'Membre',
  Tresorier = 'Tresorier',
  Secretaire = 'Secretaire',
  President = 'President',
  SuperAdmin = 'SuperAdmin',
}

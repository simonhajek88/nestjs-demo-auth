import { Module } from '@nestjs/common';
import { APP_GUARD } from '@nestjs/core';
import { MongooseModule } from '@nestjs/mongoose';
import * as config from 'config';
import { AccessModule } from './access/access.module';
import { JwtKeysService } from './access/services';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { AuthGuard } from './common';
import { PrivateController } from './private.controller';
import { PublicController } from './public.controller';
import { UserModule } from './users/user.module';

@Module({
  imports: [
    MongooseModule.forRootAsync({
      useFactory: async () => ({
        uri: config.get('mongodb.uri'),
        useNewUrlParser: true,
        useFindAndModify: false,
        useCreateIndex: true,
        useUnifiedTopology: true,
      }),
    }),
    AccessModule,
    AuthModule,
    UserModule,
  ],
  controllers: [PrivateController, PublicController],
  providers: [
    AppService,
    {
      // global guard
      provide: APP_GUARD,
      useClass: AuthGuard,
    },
  ],
})
export class AppModule {
  constructor(private readonly jwtKeysService: JwtKeysService) {
    this.jwtKeysService.initJwtKeys().subscribe();
  }
}

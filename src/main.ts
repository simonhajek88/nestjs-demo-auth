import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import {
  FastifyAdapter,
  NestFastifyApplication,
} from '@nestjs/platform-fastify';
import * as conf from 'config';
import * as multipart from 'fastify-multipart';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create<NestFastifyApplication>(
    AppModule,
    new FastifyAdapter({ logger: true }),
  );

  // register multipart plugin
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  await app.register(multipart);

  /**
   * Cookies managment from fastify
   */
  // eslint-disable-next-line @typescript-eslint/no-var-requires
  app.register(require('fastify-cookie'));
  // Collection of 12 smaller middleware functions that set security-related HTTP headers
  // app.register(helmet);

  // Use class-validator by default
  app.useGlobalPipes(
    new ValidationPipe({ whitelist: true, forbidNonWhitelisted: true }),
  );

  app.enableCors({
    credentials: true,
    origin: conf.get('application.uri'),
    exposedHeaders: 'ajax_redirect',
  });

  await app.listen(conf.get('server.port'), conf.get('server.docker-host'));
}
bootstrap();

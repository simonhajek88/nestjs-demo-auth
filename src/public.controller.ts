import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';
import { Public } from './common';

//@Public()
@Controller()
export class PublicController {
  constructor(private readonly appService: AppService) {}

  // You can put here public or also before @Controller if you have @Get, @Post etc in the same file
  @Public()
  @Get('/public')
  getHello(): string {
    return this.appService.getHello();
  }
}

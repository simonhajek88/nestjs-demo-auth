import {
  ForbiddenException,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { Observable, of, throwError } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';
import { DecodedJwt, JwtAccessService } from '../../access/services';
import { User } from '../../users/schemas/user.schema';
import { UserService } from '../../users/services/user.service';

@Injectable()
export class AuthService {
  constructor(
    private readonly userService: UserService,
    private readonly jwtService: JwtAccessService,
  ) {}

  login(
    email: string,
    password: string,
  ): Observable<{ token: string; user: User }> {
    return this.userService
      .findByEmailAndPassword(email, password)
      .pipe(
        mergeMap((user) =>
          !!user && !!user.email && user.is_activated
            ? this.jwtService
                .createJwt(user.id)
                .pipe(map((jwt) => ({ token: jwt, user })))
            : throwError(() =>
                !user
                  ? new ForbiddenException(
                      'user doesnt sexist or wrong password',
                    )
                  : new ForbiddenException('user is not activated for now'),
              ),
        ),
      );
  }

  /**
   * Resolve user by jwt
   */
  getAuthUserByJwt(value: string): Observable<any> {
    return of(value).pipe(
      mergeMap((token) => this.jwtService.verifyAndDecodeJwt(token)),
      mergeMap((decodedJwt: DecodedJwt) =>
        !!decodedJwt.aud
          ? this.userService.findById(decodedJwt.aud)
          : throwError(() => new UnauthorizedException()),
      ),
      mergeMap((user) =>
        !!user.id.toString()
          ? of(user)
          : throwError(() => new UnauthorizedException()),
      ),
    );
  }
}

import { Module } from '@nestjs/common';
import { UserModule } from '../users/user.module';
import { LoginController } from './controllers/login.controller';
import { LogoutController } from './controllers/logout.controller';
import { AuthService } from './services/auth.service';

@Module({
  providers: [AuthService],
  imports: [UserModule],
  controllers: [LoginController, LogoutController],
})
export class AuthModule {}

import { Controller, ForbiddenException, Get, Req, Res } from '@nestjs/common';
import * as config from 'config';
import * as cookie from 'cookie';
import { FastifyReply } from 'fastify';
import { of } from 'rxjs';
import { catchError, mergeMap, tap } from 'rxjs/operators';
import { JwtAccessService } from '../../access/services/jwt-access.service';
import { AuthService } from '../services/auth.service';

@Controller('/logout')
export class LogoutController {
  constructor(
    private readonly authService: AuthService,
    private readonly jwtService: JwtAccessService,
  ) {}

  @Get()
  HandlerGet(@Req() request: any, @Res() response: FastifyReply<any>) {
    // TODO do stuff to invalidate nonce and jwt be
    return of(request.headers.cookie.replace('indygoid=', '')).pipe(
      mergeMap((jwt) => this.authService.getAuthUserByJwt(jwt)),
      mergeMap((user) => this.jwtService.logout(user._id.toString())),
      tap(() =>
        response
          .header(
            'Set-Cookie',
            cookie.serialize('indygoid', '', {
              path: '/',
              domain: config.get('server.host'),
            }),
          )
          .status(204)
          .send(),
      ),
      catchError((err) => {
        response.send(new ForbiddenException(err));
        return of(err);
      }),
    );
  }
}

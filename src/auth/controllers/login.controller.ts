import {
  Body,
  Controller,
  Post,
  Res,
  UnauthorizedException,
} from '@nestjs/common';
import { IsNotEmpty } from 'class-validator';
import * as config from 'config';
import * as cookie from 'cookie';
import { FastifyReply } from 'fastify';
import { of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { Public } from '../../common/decorators/public.decorator';
import { AuthService } from '../services/auth.service';

export class UserLoginPayload {
  @IsNotEmpty()
  email: string;
  @IsNotEmpty()
  password: string;
}

@Controller('/login')
export class LoginController {
  constructor(private readonly authService: AuthService) {}

  @Public()
  @Post()
  login(
    @Res() response: FastifyReply<any>,
    @Body() userLoginPayload: UserLoginPayload,
  ) {
    return this.authService
      .login((userLoginPayload || {}).email, (userLoginPayload || {}).password)
      .pipe(
        tap((userAndToken) =>
          response
            .header(
              'Set-Cookie',
              cookie.serialize('indygoid', userAndToken.token, {
                path: '/',
                domain: config.get('server.host'),
              }),
            )
            .send(userAndToken),
        ),
        catchError((err) => {
          response.send(new UnauthorizedException('bad credentials, ' + err));
          return of(err);
        }),
      );
  }
}

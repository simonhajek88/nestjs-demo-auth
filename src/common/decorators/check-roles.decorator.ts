import { SetMetadata } from '@nestjs/common';
import { Roles } from '../../users/interfaces';

export const CHECK_ROLES_METADATA = 'roles';

// Will do a OR btw each roles provided
export const CheckRoles = (roles: Roles[]) =>
  SetMetadata(CHECK_ROLES_METADATA, roles);

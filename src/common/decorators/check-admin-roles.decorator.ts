import { SetMetadata } from '@nestjs/common';
import { AdminRoles } from '../../users/interfaces';

export const CHECK_ADMIN_ROLES_METADATA = 'admin_roles';

// Will do a OR btw each roles provided
export const CheckAdminRoles = (roles: AdminRoles[]) =>
  SetMetadata(CHECK_ADMIN_ROLES_METADATA, roles);

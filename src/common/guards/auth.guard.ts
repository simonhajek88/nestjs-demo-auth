import {
  CanActivate,
  ExecutionContext,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Observable, of, throwError } from 'rxjs';
import { catchError, mergeMap, tap } from 'rxjs/operators';
import { DecodedJwt, JwtAccessService } from '../../access/services';
import { UserService } from '../../users/services/user.service';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(
    private readonly reflector: Reflector,
    private readonly jwtService: JwtAccessService,
    private readonly userService: UserService,
  ) {}

  canActivate(context: ExecutionContext): Observable<boolean> {
    const req = context.switchToHttp().getRequest();

    const isPublic = this.reflector.get<boolean>(
      'isPublic',
      context.getHandler(),
    );

    const cookie = !!req.headers.cookie
      ? req.headers.cookie.toString().replace('indygoid=', '')
      : null;
    return isPublic
      ? of(true)
      : of(cookie).pipe(
          mergeMap((indygoid) =>
            !!indygoid
              ? this.getAuthUserByJwt(indygoid).pipe(
                  tap((user) => (req.user = user)),
                )
              : throwError(() => new UnauthorizedException()),
          ),
          catchError(() => {
            return throwError(() => new UnauthorizedException());
          }),
        );
  }

  private getAuthUserByJwt(value: string): Observable<any> {
    return of(value).pipe(
      mergeMap((token) => this.jwtService.verifyAndDecodeJwt(token)),
      mergeMap((decodedJwt: DecodedJwt) =>
        !!decodedJwt.aud
          ? this.userService.findById(decodedJwt.aud)
          : throwError(() => new UnauthorizedException()),
      ),
      mergeMap((user) =>
        !!user.id ? of(user) : throwError(() => new UnauthorizedException()),
      ),
    );
  }
}

import {
  CanActivate,
  ExecutionContext,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { of, throwError } from 'rxjs';
import { AdminRoles, Roles } from '../../users/interfaces';
import { User } from '../../users/schemas/user.schema';
import {
  CHECK_ADMIN_ROLES_METADATA,
  CHECK_ROLES_METADATA,
  IS_PUBLIC_METADATA,
} from '../decorators';

@Injectable()
export class RolesGuard implements CanActivate {
  constructor(private readonly reflector: Reflector) {}

  canActivate(context: ExecutionContext) {
    // If the route is tagged as public let it go and return true
    const isPublic =
      this.reflector.get<boolean>(IS_PUBLIC_METADATA, context.getHandler()) ||
      this.reflector.get<boolean>(IS_PUBLIC_METADATA, context.getClass());

    if (isPublic) {
      return of(true);
    }

    const req = context.switchToHttp().getRequest();
    const user = req.user;
    // Unique array of roles provided by class decorator and handler decorator
    const rolesToCheck: Roles[] = [
      ...new Set(
        ([] as Roles[])
          .concat(
            this.reflector.get<Roles[]>(
              CHECK_ROLES_METADATA,
              context.getHandler(),
            ),
          )
          .concat(
            this.reflector.get<Roles[]>(
              CHECK_ROLES_METADATA,
              context.getClass(),
            ),
          )
          .filter(Boolean),
      ),
    ];

    const adminRolesToCheck: AdminRoles[] = [
      ...new Set(
        ([] as AdminRoles[])
          .concat(
            this.reflector.get<AdminRoles[]>(
              CHECK_ADMIN_ROLES_METADATA,
              context.getHandler(),
            ),
          )
          .concat(
            this.reflector.get<AdminRoles[]>(
              CHECK_ADMIN_ROLES_METADATA,
              context.getClass(),
            ),
          )
          .filter(Boolean),
      ),
    ];
    const isAuthorized =
      this.checkRoles(user, rolesToCheck) &&
      this.checkAdminRoles(user, adminRolesToCheck);

    if (!isAuthorized) {
      return throwError(
        () =>
          new UnauthorizedException('role granted for the user is not enough'),
      );
    }

    return of(
      this.checkRoles(user, rolesToCheck) &&
        this.checkAdminRoles(user, adminRolesToCheck),
    );
  }

  /**
   * Verify that the user has at least one of rolesToCheck
   * @param user the current user doing the request
   * @param rolesToCheck the array of roles that we have to check
   * @returns true if yes false if not
   */
  private checkRoles(user: User, rolesToCheck: Roles[]): boolean {
    return (
      !rolesToCheck.length ||
      !!rolesToCheck.find(
        (role) => role.toLowerCase() === user.role.toLowerCase(),
      )
    );
  }

  /**
   * Verify that the user has at least one of rolesToCheck
   * @param user the current user doing the request
   * @param rolesToCheck the array of roles that we have to check
   * @returns true if yes false if not
   */
  private checkAdminRoles(
    user: User,
    adminRolesToCheck: AdminRoles[],
  ): boolean {
    return (
      !adminRolesToCheck.length ||
      !!adminRolesToCheck.find(
        (role) => role.toLowerCase() === user.admin_role.toLowerCase(),
      )
    );
  }
}

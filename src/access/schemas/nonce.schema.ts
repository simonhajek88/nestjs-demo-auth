import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type NonceDocument = Nonce & Document;

@Schema({
  timestamps: {
    updatedAt: 'updated_at',
    createdAt: 'created_at',
  },
})
export class Nonce {
  @Prop()
  user_id: string;
  @Prop()
  nonce: string;
  @Prop()
  expiration_date: Date;
  @Prop()
  invalidate_date: Date;
  id: string;
}

export const NonceSchema = SchemaFactory.createForClass(Nonce);

NonceSchema.set('toJSON', {
  virtuals: true,
  transform(doc, ret) {
    ret.id = doc._id.toString();
    delete ret._id;
    delete ret.__v;
    return ret;
  },
});

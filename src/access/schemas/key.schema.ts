import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
export type SecureKeyDocument = SecureKey & Document;

@Schema({
  timestamps: {
    updatedAt: 'updated_at',
    createdAt: 'created_at',
  },
})
export class SecureKey {
  @Prop()
  pub_key: string;
  @Prop()
  priv_key: string;
  id: string;
}

export const SecureKeySchema = SchemaFactory.createForClass(SecureKey);

SecureKeySchema.set('toJSON', {
  virtuals: true,
  transform(doc, ret) {
    ret.id = doc._id.toString();
    delete ret._id;
    delete ret.__v;
    return ret;
  },
});

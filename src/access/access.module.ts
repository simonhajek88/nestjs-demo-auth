import { Module, Global } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { JwtNoncesService, JwtAccessService, JwtKeysService } from './services';
import { CryptoModule } from '@akanass/nestjsx-crypto';
import { Nonce, NonceSchema, SecureKey, SecureKeySchema } from './schemas';

@Global()
@Module({
  imports: [
    MongooseModule.forFeature([
      { name: SecureKey.name, schema: SecureKeySchema },
    ]),
    MongooseModule.forFeature([{ name: Nonce.name, schema: NonceSchema }]),
    CryptoModule,
  ],
  controllers: [],
  providers: [JwtKeysService, JwtNoncesService, JwtAccessService],
  exports: [JwtAccessService, JwtKeysService],
})
export class AccessModule {}

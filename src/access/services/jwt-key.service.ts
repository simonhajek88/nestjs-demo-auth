import { AesService, PemService } from '@akanass/nestjsx-crypto';
import {
  decryptWithAesKey,
  encryptWithAesKey,
} from '@akanass/nestjsx-crypto/operators/aes';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import * as config from 'config';
import { Model } from 'mongoose';
import { from, Observable, of } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';
import { SecureKey } from '../schemas/key.schema';

@Injectable()
export class JwtKeysService {
  private aesKey: any;
  constructor(
    private readonly pemService: PemService,
    private readonly aesService: AesService,
    @InjectModel(SecureKey.name)
    private readonly secureKeyModel: Model<SecureKey>,
  ) {
    this.initAesKey().subscribe((key) => (this.aesKey = key));
  }

  private initAesKey() {
    return from(
      this.aesService.createKey(
        config.get('security.password'),
        config.get('security.salt'),
      ),
    );
  }

  getKeyPair(): Observable<SecureKey> {
    return from(this.secureKeyModel.findOne());
  }

  getPrivateKey(): Observable<string> {
    return from(this.secureKeyModel.findOne()).pipe(
      map((keypair) => keypair.priv_key),
      mergeMap((privKey) => this.decryptPrivateKey(privKey)),
    );
  }

  getPublicKey(): Observable<string> {
    return from(this.secureKeyModel.findOne()).pipe(
      map((keypair) => keypair.pub_key),
    );
  }

  /**
   * Initialize the keypair :
   * Generate it if not exists
   */
  initJwtKeys() {
    return from(this.secureKeyModel.find()).pipe(
      mergeMap((keypair) =>
        [].concat(keypair).length > 0
          ? of(keypair)
              .pipe
              // tap(_ => this.logger.value.info(`Keypair already generated`))
              ()
          : this.generateAndSaveKeypair(),
      ),
    );
  }

  /**
   * Generate and save in db a keypair
   */
  private generateAndSaveKeypair(): Observable<SecureKey> {
    return from(this.pemService.createKeyPair()).pipe(
      mergeMap((keypair) =>
        this.encryptPrivateKey(keypair.key).pipe(
          mergeMap((encryptedKey) =>
            from(
              this.secureKeyModel.create({
                pub_key: keypair.publicKey,
                priv_key: encryptedKey,
              }),
            ),
          ),
        ),
      ),
    );
  }

  /**
   * Encrypt the private key
   * @param privateKey the privateKey to encrypt
   */
  private encryptPrivateKey(privateKey: string): Observable<string> {
    return of(this.aesKey).pipe(
      encryptWithAesKey(Buffer.from(privateKey)),
      map((bufferEncrypted) => bufferEncrypted.toString('hex')),
    );
  }

  /**
   * Decrypt the private key with aes
   * @param encryptedPrivateKey
   */
  private decryptPrivateKey(encryptedPrivateKey: string): Observable<string> {
    return of(this.aesKey).pipe(
      decryptWithAesKey(Buffer.from(encryptedPrivateKey, 'hex')),
      map((decrypted) => decrypted.toString()),
    );
  }
}

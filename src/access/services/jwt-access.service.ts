import { JwtService } from '@akanass/nestjsx-crypto';
import { Injectable } from '@nestjs/common';
import * as config from 'config';
import { Observable, of } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import * as Uuid from 'uuid';
import { JwtKeysService } from './jwt-key.service';
import { JwtNoncesService } from './jwt-nonce.service';

export class JwtOptions {
  audience: string;
  issuer: string;
  jwtid: string;
  expiresIn: number;
}

export class DecodedJwt {
  iat: number;
  aud: string;
  iss: string;
  jti: string;
}

@Injectable()
export class JwtAccessService {
  constructor(
    private readonly jwtService: JwtService,
    private readonly jwtNonceService: JwtNoncesService,
    private readonly jwtKeysService: JwtKeysService,
  ) {}

  /**
   * Create jwt for one user and one connexion
   * @param user_id
   */
  createJwt(user_id: string) {
    return of(Uuid.v1()).pipe(
      mergeMap((uuid) => this.jwtNonceService.create(uuid, user_id)),
      mergeMap((nonce) => this.signJwt(user_id, nonce.nonce)),
    );
  }

  /**
   * Generate a jwt token with options provided by prepareOptions function
   * @param user_id
   * @param nonce unique uid associated with one jwt
   */
  signJwt(user_id: string, nonce: string) {
    return this.jwtKeysService
      .getPrivateKey()
      .pipe(
        mergeMap((privateKey) =>
          this.prepareOptions(user_id, nonce).pipe(
            mergeMap((options) =>
              this.jwtService.sign({}, privateKey, options),
            ),
          ),
        ),
      );
  }

  /**
   * Verify the integrity for the token and check if token is valid
   * @param token the jwt token
   */
  verifyAndDecodeJwt(token: string) {
    return this.jwtKeysService.getPublicKey().pipe(
      mergeMap((publicKey) =>
        this.jwtService.verify(token, publicKey, {
          algorithms: [config.get('jwtconf.algorithm')],
        }),
      ),
      mergeMap((jwtDecoded: DecodedJwt) =>
        this.jwtNonceService.verifyNonce(jwtDecoded),
      ),
    );
  }

  /**
   * Prepare options to generate the jwt
   * @param user_id
   * @param nonce
   */
  private prepareOptions(
    user_id: string,
    nonce: string,
  ): Observable<JwtOptions> {
    return of({
      algorithm: config.get('jwtconf.algorithm'),
      audience: user_id.toString(),
      issuer: config.get('jwtconf.issuer'),
      jwtid: nonce,
      expiresIn: config.get('jwtconf.duration_time'),
    });
  }

  /**
   * logout for one user
   * @param user_id
   */
  logout(user_id: string) {
    return of(user_id).pipe(
      mergeMap(() =>
        this.jwtNonceService.invalidateAllNoncesForOneUser(user_id),
      ),
    );
  }
}

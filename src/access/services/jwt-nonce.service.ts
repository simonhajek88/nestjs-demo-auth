import { Injectable, UnauthorizedException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import * as config from 'config';
import { Model } from 'mongoose';
import { from, Observable, of, throwError } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';
import { Nonce } from '../schemas/nonce.schema';
import { DecodedJwt } from './jwt-access.service';

@Injectable()
export class JwtNoncesService {
  constructor(
    @InjectModel(Nonce.name) private readonly nonceModel: Model<Nonce>,
  ) {}

  /**
   * Create a nonce for a user
   * By default
   */
  create(nonce: string, user_id: string): Observable<Nonce> {
    const now = new Date();
    const jwt_duration = config.get('jwtconf.duration_time') as number;
    return from(
      this.nonceModel.create({
        user_id,
        nonce,
        expiration_date: new Date(
          now.setSeconds(now.getSeconds() + jwt_duration),
        ),
      } as Nonce),
    );
  }

  /**
   * Verify the validity of the nouce associated with the jwt
   * @param decodedJwt jwt decoded
   */
  verifyNonce(decodedJwt: DecodedJwt): Observable<DecodedJwt> {
    return this.getNonceByJti(decodedJwt.jti).pipe(
      mergeMap((nonce) => this.isNonceValid(nonce)),
      mergeMap((nonce) =>
        of(decodedJwt).pipe(
          mergeMap(() => this.updateExpirationDate(nonce.id)),
          map(() => decodedJwt),
        ),
      ),
    );
  }

  private isNonceValid(nonce: Nonce): Observable<Nonce> {
    return !nonce.invalidate_date && nonce.expiration_date > new Date()
      ? of(nonce)
      : throwError(new UnauthorizedException('Nonce has expired'));
  }

  private getNonceByJti(jti: string): Observable<Nonce> {
    return from(this.nonceModel.findOne({ nonce: jti })).pipe(
      mergeMap((nonce) =>
        !!nonce
          ? of(nonce)
          : throwError(() => new UnauthorizedException('Nonce not exists')),
      ),
    );
  }

  /**
   * Update the expiration date for a specific nonce, add seconds to now date
   * @param nonce_id
   */
  updateExpirationDate(nonce_id: string): Observable<Nonce> {
    const now = new Date();
    const jwt_duration = 2419200;
    return from(
      this.nonceModel.findByIdAndUpdate(nonce_id, {
        $set: {
          expiration_date: new Date(
            now.setSeconds(now.getSeconds() + jwt_duration),
          ),
        },
      }),
    );
  }

  /**
   * invalidate all nonces for one user (by user id)
   * @param user_id
   */
  invalidateAllNoncesForOneUser(user_id: string) {
    return from(
      this.nonceModel.updateMany(
        { user_id },
        { $set: { invalidate_date: new Date() } },
      ),
    );
  }
}
